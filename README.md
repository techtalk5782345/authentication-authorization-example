# Authentication & Authorization Example

## 1. Project Description

This repository serves as a demonstration of authentication & authorization in a Spring Boot application using spring security.

## 2. Prerequisites

Before you start, ensure you have the following requirements in place:

- Java Development Kit (JDK) installed
- Git installed
- An Integrated Development Environment (IDE) such as IntelliJ IDEA or Eclipse
- Maven installed (for dependency management)

## 3. Getting Started

1. Clone the Repository:

```
git clone https://gitlab.com/techtalk5782345/authentication-authorization-example.git
```

2. Navigate to the project directory:

```
cd authentication_authorization_example
```

3. Run the Spring Boot Application

``` 
./mvnw spring-boot:run
```

4. Once Spring Boot Application is started. You can hit the below API

```
http://localhost:8080/authenticate
```

5. Once you hit the above API. You will get the JWT token in response which you can use in the request header in order to hit the below API.

```
http://localhost:8080/greetings

Authorization: Bearer 'JWT token which you got in the response of above API'
```

6. After you hit the API with valid JWT token, then you will see ```Hello Developer``` string in response.